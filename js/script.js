"use strict";

const sec = document.querySelector(".sec");
const min = document.querySelector(".min");
const hour = document.querySelector(".hour");
const buttonStart = document.querySelector(".start");
const buttonClear = document.querySelector(".clear");
const buttonPause = document.querySelector(".pause");

let counterSec = 0;
let counterMin = 0;
let counterHour = 0;
let isClick = false;
let isPause = true;
let isClickPauseButton = false;
let isClickClearButton = false;

let click_1;
let click_2;
let timer;

sec.textContent = addZero(counterSec);
min.textContent = addZero(counterMin);
hour.textContent = addZero(counterHour);

buttonStart.addEventListener("click", () => {
   isClick = !isClick;
   startTimer();
});

buttonClear.addEventListener("click", () => {
   clearTimer();
});

buttonPause.addEventListener("click", () => {
   isDoubleClick();
});


function isDoubleClick() {

   if (!isClickPauseButton) {
      isClickPauseButton = true;
      click_1 = new Date().getTime();
      // console.log(`click_1 ${click_1}`);

   } else if (isClickPauseButton) {
      click_2 = new Date().getTime();
      let delta = click_2 - click_1;
 
      if (delta < 300) {
         stopTimer();
         isClickPauseButton = false;
         // console.log(`click_1 ${click_1}`);
         // console.log(`click_2 ${click_2}`);
         // console.log(delta);

      } else {
         click_1 = new Date().getTime();
         // console.log(`click_1 ${click_1}`);
         // console.log(`click_2 ${click_2}`);
         // console.log(delta);
      }
   }
};

function clearTimer() {
  
   if (isPause && !isClickClearButton) {
      counterSec = 0;
      counterMin = 0;
      counterHour = 0;
      display(sec);
      display(min);
      display(hour);
      buttonClear.style.color = 'yellowgreen';
      buttonPause.style.color = 'rgb(209,209,217)';
      buttonStart.style.color = 'rgb(209,209,217)';
      isClickClearButton = !isClickClearButton;

   } else {
      buttonClear.style.color = 'rgb(209,209,217)';
   }
};

function stopTimer() {
  
   if (!isPause) {
      clearInterval(timer);
      isPause = true;
      isClick = false;
      buttonPause.style.color = 'yellowgreen';
      buttonClear.style.color = 'rgb(209,209,217)';
      buttonStart.style.color = 'rgb(209,209,217)';
   }
};

function startTimer() {
   
   if (isClick && isPause) {
      timer = setInterval(counter, 10);
      isPause = false;
      buttonStart.style.color = 'yellowgreen';
      buttonPause.style.color = 'rgb(209,209,217)';
      buttonClear.style.color = 'rgb(209,209,217)';
      isClickClearButton = false;
   }
};

function addZero(i) {
   return i = i < 10 ? `0 ${i}` : i;
}

function display(i) {
   
   if (i === sec) {
      return i.textContent = addZero(counterSec);
   } else if (i === min) {
      return i.textContent = addZero(counterMin);
   } else {
      return i.textContent = addZero(counterHour);
   }
}

function counter() {
   
   counterSec++;

   if (counterSec > 59) {
      counterSec = 0;
      counterMin++;

      display(sec);
      display(min);

      if (counterMin > 59) {
         counterMin = 0;
         counterHour++;

         display(min);
         display(hour);
      }
   }
   display(sec);
}

